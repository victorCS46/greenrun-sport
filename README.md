# Greenrun Sports App

This is a Tinder-like PWA where instead of coupling you with people you get couple with sports that you might like.

## Requirements

NodeJS >= v17.2.0

## Installation

- **Step.1:** Clone this repository. 

`git clone https://gitlab.com/victorCS46/greenrun-sport.git`

- **Step.2:** Change to project directory

`cd greenrun-sport`

- **Step.3:** Install dependencies using yarn

`yarn install`

- **Step.4:** Deploy your app in localhost

`yarn start`

Now you are ready to try this app by yourself in 🚀:  http://localhost:3000/

Enjoy :)

## Testing credentials

You can use this test user in case that you don't want to sign up as new user:

```
email: test@greenrun.com
password: helloworld
```
