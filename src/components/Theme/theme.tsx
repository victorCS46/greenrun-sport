export const lightTheme = {
  body: '#e9e9e9',
  text: '#363537',
  lightText: '#5c5c5c',
  background: '#FFF',
}; 

export const darkTheme = {
  body: '#232232',
  text: '#FAFAFA',
  lightText: '#FAFAFA',
  background: '#2C2B3E',
};
