import React from 'react';

export const DarkContext = React.createContext<any>({});

const DarkProvider = ({ children }: any): JSX.Element => {

  const [isDark, setIsDark] = React.useState<boolean>(false);

  return (
    <DarkContext.Provider value={{ isDark, setIsDark }}>
      { children }
    </DarkContext.Provider>
  );
};

export default DarkProvider;