import React from 'react';

export const AuthContext = React.createContext<any>({});

const AuthProvider = ({ children }: any) => {

  const [user, setUser] = React.useState<string | null>(null);

  React.useEffect(() => {
    if ( !user ) {
      setUser(localStorage.getItem('authToken'));
    }
  }, [user]);

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      { children }
    </AuthContext.Provider>
  )
};

export default AuthProvider;