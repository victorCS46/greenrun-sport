import React from 'react';
import { RiHomeFill } from "react-icons/ri";
import { AiOutlineClockCircle } from "react-icons/ai";
import { BiNotepad } from "react-icons/bi";
import { useLocation, useNavigate } from 'react-router';
import { useTheme } from 'styled-components';

const BottomTabBar = (): JSX.Element => {
  
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const theme: any = useTheme();

  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}>
      <div style={{
        position: 'absolute',
        bottom: 20,
        backgroundColor: theme.background,
        height: 100,
        width: '90%',
        borderRadius: 25,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
      }}>
        <div onClick={() => navigate('/home')}>
          <RiHomeFill 
            size={30}
            color={pathname === '/home' ? '#2067F8' : theme.text}
          />
        </div>
        <div onClick={() => navigate('/history')}>
          <AiOutlineClockCircle
            size={30}
            color={pathname === '/history' ? '#2067F8' : theme.text}
          />
        </div>
        <div>
          <BiNotepad
            size={30}
            color={theme.text}
          />
        </div>
        <div onClick={() => navigate('/profile')}>
          <img
            width={25}
            height={25}
            src={require('../../assets/images/profile.png')}
            alt="icon"
          />
        </div>
      </div>
    </div>
  );
};

export default BottomTabBar;