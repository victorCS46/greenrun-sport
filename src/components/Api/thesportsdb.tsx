import axios from 'axios';

const APIKEY = 2; // thesportsdb's API key for testing

const theSportsdb = axios.create({
  baseURL: `https://www.thesportsdb.com/api/v1/json/${ APIKEY }`,
});

export const getAllSports = async () => {
  return await theSportsdb.get('/all_sports.php');
};

export default theSportsdb;
