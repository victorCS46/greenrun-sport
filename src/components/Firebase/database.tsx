import { onValue, ref, set } from '@firebase/database';
import { database } from '../../firebase-config';
import uid from '../Helpers/uid';

// store the selected option by the user
export const storeHistory = (email: string, data: any) => {

  set(ref(database, `history/${ email }/${ uid() }`), data); 

};

// Read all user's history
export const retrieveHistory = (email: string) => {

  const history = ref(database, `history/${ email }`);

  let retrieved: any = [];

  onValue(history, (snapshot) => {
    snapshot.forEach((snap) => {
      retrieved.push(snap.val())
    });
  }, {
    onlyOnce: true
  });

  return retrieved;
};