import React from 'react';
import { getAllSports } from '../../Api/thesportsdb';
import { AuthContext } from '../../Context/AuthContext';
import { Dislike, Image, Like, SportIcon, Title, ToogleTheme } from './homeStyledComp';
import jwtDecode from 'jwt-decode';
import { storeHistory } from '../../Firebase/database';
import BottomTabBar from '../../Shared/BottomTabBar';
import { DarkContext } from '../../Context/DarkContext';

interface Sportdb {
  idSport: string;
  strFormat: string;
  strSport: string;
  strSportDescription: string;
  strSportIconGreen: string;
  strSportThumb: string;
};

const Home = (): JSX.Element => {

  const [sports, setSports] = React.useState<Sportdb[]>([]);
  const [index, setIndex] = React.useState(0);
  const [email, setEmail] = React.useState('');
  
  const { user } = React.useContext(AuthContext);
  const { isDark, setIsDark } = React.useContext(DarkContext);

  React.useEffect(() => {
    getAllSports()
      .then(({ data }) => {setSports(data.sports);})
      .catch(( err ) => console.log(err));
    
    if (user) {
      const jwt = jwtDecode<any>(user);
      const email = jwt.email.substring(0, jwt.email.indexOf('@'));
      setEmail(email);
    }
  }, [user]);

  const { 
    idSport,
    strSport,
    strSportThumb,
    strSportIconGreen,
  } = sports[index] || {};

  const next = () => setIndex(index + 1);

  return (
    <>
      <div key={ idSport }>
        <div style={{position: 'relative'}}>
          <ToogleTheme onClick={() => setIsDark(!isDark)}>
            <img
              height={25}
              width={25}
              src={
                !isDark
                ? require('../../../assets/images/dark.png')
                : require('../../../assets/images/light.png')
              }
              alt="no_img"
            />
          </ToogleTheme>
          <SportIcon>
            <img
              height={40}
              width={40}
              src={ strSportIconGreen }
              alt="no_img"
            />
          </SportIcon>
          <Image
            src={ strSportThumb }
            alt="no_img"
          />
          <Title>{ strSport }</Title>
        </div>
        <div style={{position: 'relative'}}>
          <div style={{
            marginTop: 80,
            display: 'flex',
            justifyContent: 'space-around',
          }}>
            <Dislike 
              onClick={() => {
                storeHistory(email, {
                  sportName: strSport,
                  reaction: 'dislike',
                  reactedAt: Date.now(),
                  sportImgURL: strSportThumb,
                });
                next();
              }} 
              style={{width: 80, height: 60}}
            >
              <img 
                src={require('../../../assets/images/dislike.png')}
                alt="no_img"
              />
            </Dislike>
            <Like
              onClick={() => {
                storeHistory(email, {
                  sportName: strSport,
                  reaction: 'like',
                  reactedAt: Date.now(),
                  sportImgURL: strSportThumb,
                });
                next();
              }} 
              style={{width: 80, height: 60}}
            >  
              <img
                src={require('../../../assets/images/like.png')} 
                alt="no_img"
              />
            </Like>
          </div>
        </div>
      </div>
      <BottomTabBar />
    </>
  )
}

export default Home;