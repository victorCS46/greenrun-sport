import styled from "styled-components";

export const Image = styled.img`
  width: 100%;
  height: 55vh;
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
`;

export const ToogleTheme = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 60px;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  backdrop-filter: blur(20px);
  border-radius: 18px;
`;

export const SportIcon = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  width: 60px;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  backdrop-filter: blur(20px);
  border-radius: 18px;
`;

export const Title = styled.h2`
  font-family: 'DM Sans';
  font-style: normal;
  font-weight: 700;
  font-size: 34px;
  line-height: 122.02%;
  position: absolute;
  bottom: 10px;
  left: 10px;
  color: white;
`;

export const Like = styled.div`
  // width: 60px;
  // height: 0px:
  padding-bottom: 10px;
  padding-top: 10px;
  display: flex;
  border-radius: 50%;
  text-align: center;
  justify-content: center;
  align-items: center;
  background: linear-gradient(125.02deg, #236BFE -17.11%, #063BA8 98.58%);
  box-shadow: 0px 10px 25px rgba(35, 107, 254, 0.2);
`;

export const Dislike = styled.div`
  // width: 60px;
  // height: 0px:
  padding-bottom: 10px;
  padding-top: 10px;
  display: flex;
  border-radius: 50%;
  text-align: center;
  justify-content: center;
  align-items: center;
  background: #FFFFFF;
  box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.08);
`;