import React from 'react'
import { AuthContext } from '../../Context/AuthContext';
import BottomTabBar from '../../Shared/BottomTabBar';
import { Button } from '../SignUp/signUpStyledComp';

const Profile = () => {

  const { setUser } = React.useContext(AuthContext);

  return (
    <>
      <div style={{
        display: 'flex',
        alignItems:'center',
        justifyContent: 'center',
        height: '90vh'
      }}>
        <Button
          onClick={() => setUser(localStorage.removeItem('authToken'))}
        >
          Sign Out
        </Button>
      </div>
      <BottomTabBar />
    </>
  )
}

export default Profile;