import React from 'react';
import {
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import { ThemeProvider } from 'styled-components';
import { AuthContext } from '../Context/AuthContext';
import { DarkContext } from '../Context/DarkContext';
import { GlobalStyles } from '../Theme/globalStyles';
import { darkTheme, lightTheme } from '../Theme/theme';
import History from './History/History';
import Home from './Home/Home';
import Login from './Login/Login';
import OnBoarding from './OnBoarding/OnBoarding';
import Profile from './Profile/Profile';
import SignUp from './SignUp/SignUp';

const AppRoutes = () => {

  const { isDark } = React.useContext(DarkContext);
  const { user } = React.useContext(AuthContext);
  const [token, setToken] = React.useState<string | null>(null);
  const getToken = () => setToken(localStorage.getItem('authToken'));

  React.useEffect(() => {
    getToken();
  }, [token]);

  return (
    <ThemeProvider theme={ !isDark ? lightTheme : darkTheme }>
      <GlobalStyles />
      <Routes>
        {
          (!user) ? (
            <>
              <Route path="/" element={ <OnBoarding /> } />
              <Route path="/login" element={ <Login /> } />
              <Route path="/register" element={ <SignUp /> } />
              <Route path="*" element={<Navigate to="/" replace />} />
            </>
          ) : (
            <>
              <Route path="/home" element={ <Home /> } />
              <Route path="/history" element={ <History /> } />
              <Route path="/profile" element={ <Profile /> } />
              <Route path="*" element={<Navigate to="/home" replace />} />
            </>
          )
        }
      </Routes>
    </ThemeProvider>
  )
}

export default AppRoutes;