import React from 'react';
import { ChangeEvent } from 'hoist-non-react-statics/node_modules/@types/react';
import { app } from '../../../firebase-config';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import { useNavigate } from 'react-router';
import { AuthContext } from '../../Context/AuthContext';
import { Anchor, Button, Paragraph, TextInput, Title } from './loginStyledComp';
import { useTheme } from 'styled-components';

interface LoginForm {
  email: string;
  pwd: string;
};

const Login = (): JSX.Element => {

  const [loginForm, setLoginForm] = React.useState<LoginForm>({
    email: '',
    pwd: '',
  });
  const { user, setUser } = React.useContext(AuthContext);
  const theme: any = useTheme();
  const navigate = useNavigate();
  
  const firebase = app;

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setLoginForm({
      ...loginForm,
      [event.target.name]: event.target.value,
    });
  };

  const handleLogin = ({ email, pwd }: LoginForm) => {
    if ( email === '' || pwd ==='' ) {
      alert('All fields must filled');
      return;
    }

    const auth = getAuth(firebase);
    
    signInWithEmailAndPassword(auth, email, pwd)
      .then((res: any) => {
        localStorage.setItem('authToken', res.user.accessToken);
        setUser(localStorage.getItem('authToken'));
        
        if ( !user ) {
          navigate('/home');
        } else {
          console.log('Something has gone wrong');
        }
      })
      .catch((err) => {
        alert('Login error: check if you credentials are correct');
        console.log(err);
      }); 
  };

  return (
    <div style={{
      backgroundColor: theme.body,
      display:'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
    }}>
      <div style={{
        backgroundColor: theme.body,
        position: 'absolute',
        top: '15%',
      }}>
        <Title>Welcome</Title>
        <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
          <div>
            <TextInput
              name="email"
              placeholder="User"
              type="text"
              onChange={(event) => handleChange(event)}
              style={{
                backgroundColor: theme.background,
                color: theme.text,
              }}
            />
          </div>
          <div>
            <TextInput
              name="pwd"
              placeholder="Password"
              type="password"
              onChange={(event) => handleChange(event)}
              style={{
                backgroundColor: theme.background,
                color: theme.text,
              }}
            />
          </div>
        </div>
        <div style={{marginTop: 30, marginLeft: 20,}}>
          <Anchor style={{color: theme.lightText}} href="#">Forgot your password?</Anchor>
        </div>
        <div style={{marginTop: 30, marginLeft: 20,}}>
          <Anchor style={{color: theme.lightText}} href="/register">Do not have an account yet? Sign up here</Anchor>
        </div>
        <div style={{marginTop: 30, marginLeft: 20,}}>
          <Button onClick={() => handleLogin(loginForm)}>
            Login
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Login;