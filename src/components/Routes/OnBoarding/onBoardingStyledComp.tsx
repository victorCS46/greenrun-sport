import styled from 'styled-components';

export const Title = styled.p`
  font-family: DM Sans;
  font-style: normal;
  font-weight: 700;
  font-size: 28px;
  text-align: start;
`;

export const Desc = styled.p`
  font-family: Epilogue;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
`;

export const Button = styled.button`
  background: linear-gradient(99deg, #236BFE 6.69%, #0D4ED3 80.95%);
  box-shadow: 0px 4px 30px rgba(34, 105, 251, 0.8);
  border-radius: 25px;
  padding: 22px 38px;
  color: white;
  font-family: sans-serif;
  font-weight: 700;
  font-size: 18px;
`;