import React from 'react';
import { Link } from 'react-router-dom';
import { useTheme } from 'styled-components';
import './onBoarding.css';
import { Button } from './onBoardingStyledComp';

const OnBoarding = (): JSX.Element => {

  const theme: any = useTheme();

  return (
    <div className="container">
      <div className="img-container">
        <img 
          src={ require('../../../assets/images/messi_bg.png') }
          alt="no img"
          className="messi-img"
        />
      </div>
      <div className="box" style={{backgroundColor: theme.background}}>
        <div className="text-container">
          <p className="text-title" style={{color: theme.text}}>Discover Your Best Sport With Us</p>
          <p className="text-desc" style={{color: theme.lightText}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div className="btn-container">
          <Link to="login">
            <Button>
              Login
            </Button>
          </Link>
          <Link to="register">
            <Button style={{
              background: 'linear-gradient(99deg, grey 6.69%, rgba(0, 0, 0, 0.4) 90.95%)',
              marginLeft: 20,
            }}>
              Sign up
            </Button>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default OnBoarding;