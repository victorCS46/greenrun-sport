import React from 'react';
import { ChangeEvent } from 'hoist-non-react-statics/node_modules/@types/react';
import { app } from '../../../firebase-config';
import { createUserWithEmailAndPassword, getAuth } from 'firebase/auth';
import { useNavigate } from 'react-router';
import { AuthContext } from '../../Context/AuthContext';
import { Button, Paragraph, TextInput, Title } from './signUpStyledComp';
import { HiOutlineArrowLeft } from 'react-icons/hi';
import { useTheme } from 'styled-components';
import { DarkContext } from '../../Context/DarkContext';


interface SignUpForm {
  email: string;
  pwd: string;
};

const SignUp = (): JSX.Element => {

  const [signUpForm, setSignUpForm] = React.useState<SignUpForm>({
    email: '',
    pwd: '',
  });
  const { user, setUser } = React.useContext(AuthContext);
  const { isDark } = React.useContext(DarkContext);
  const navigate = useNavigate();
  const theme: any = useTheme();
  
  const firebase = app;

  React.useEffect(() => {}, []);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSignUpForm({
      ...signUpForm,
      [event.target.name]: event.target.value,
    });
  };

  const validateEmail = (inputText: string) => {
    //eslint-disable-next-line
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.match(mailformat)) {
      return true;
    } else {
      return false;
    }
  }

  const handleSignUp = ({ email, pwd }: SignUpForm) => {
    
    if (!validateEmail(email)) {
      alert('User must have email format');
    } else if(pwd.length < 6) {
      alert('Password must have a least 6 characters');
    } else {
      const auth = getAuth(firebase);
      
      createUserWithEmailAndPassword(auth, email, pwd)
        .then((res: any) => {
          localStorage.setItem('authToken', res.user.accessToken);
          setUser(localStorage.getItem('authToken'));
          
          if ( !user ) {
            alert('Registered successfully');
            navigate('/home');
          } else {
            alert('Something has gone wrong while signing up');
          }
        })
        .catch((err) => {
          console.log(err);
        }); 
    }
    
  };

  return (
    <div style={{
      backgroundColor: theme.body,
      display:'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
    }}>
      <HiOutlineArrowLeft
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          margin: 20,
          marginTop: 50
        }}
        onClick={() => navigate('/')}
        size={30}
        color={!isDark ? 'black' : 'white'}
      />
      <div style={{
        backgroundColor: theme.body,
        position: 'absolute',
        top: '15%',
      }}>
        <Title>Welcome</Title>
        <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Paragraph>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
          <div>
            <TextInput
              name="email"
              placeholder="User"
              type="text"
              onChange={(event) => handleChange(event)}
              style={{
                backgroundColor: theme.background,
                color: theme.text,
              }}
            />
          </div>
          <div>
            <TextInput
              name="pwd"
              placeholder="Password"
              type="password"
              onChange={(event) => handleChange(event)}
              style={{
                backgroundColor: theme.background,
                color: theme.text,
              }}
            />
          </div>
        </div>
        {/* <div style={{marginTop: 30, marginLeft: 20,}}>
          <Anchor href="#">Forgot your password?</Anchor>
        </div> */}
        <div style={{marginTop: 30, marginLeft: 20,}}>
          <Button onClick={() => handleSignUp(signUpForm)}>
            Register
          </Button>
        </div>
      </div>
    </div>
  );
};

export default SignUp;