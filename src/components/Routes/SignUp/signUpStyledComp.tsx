import styled from 'styled-components';

export const Title = styled.h1`
  text-align: center;
  font-family: 'DM sans';
  font-style: normal;
  font-weight: 700;
  font-size: 42px;
  line-height: 122.02%;
`;

export const Paragraph = styled.p`
  text-align: center;
  font-family: 'Epilogue';
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 148.02%;
  // color: #232232;
`;

export const TextInput = styled.input`
  background-color: #FFFFFF;
  border: 1px solid rgba(0, 0, 0, 0.5);
  box-sizing: border-box;
  border-radius: 18px;
  width: 329px;
  height: 67px;
  margin-bottom: 10px;
  text-indent: 10px;
  font-family: DM Sans;
  font-weight: 400;
  font-size: 18px;
`;

export const Anchor = styled.a`
  font-family: 'DM Sans';
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 122.02%;
  // color: rgba(35, 34, 50, 0.8);
`;

export const Button = styled.button`
  background: linear-gradient(99deg, #236BFE 6.69%, #0D4ED3 80.95%);
  box-shadow: 0px 4px 30px rgba(34, 105, 251, 0.8);
  border-radius: 25px;
  padding: 22px 38px;
  color: white;
  font-family: sans-serif;
  font-weight: 700;
  font-size: 18px;
`;