import styled from "styled-components";

export const Title = styled.h2`
  font-family: DM Sans;
  font-weight: 700;
  font-size: 28px;
`;

export const Desc = styled.p`
  font-family: Epilogue;
  font-weight: 400;
  font-size: 18px;
  // color: rgba(0, 0, 0, 0.6);
`;

export const ItemContainer = styled.div`
  background-color: #FFFFFF;
  height: 80px;
  width: 95%;
  border-radius: 25px;
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  position: relative;
`;

export const ItemTitle = styled.h3`
  position: absolute;
  bottom: 0px;
  top: 10px;
  left: 5px;
  color: white;
  font-family: DM sans;
  font-weight: 700;
  font-size: 24px;
`;