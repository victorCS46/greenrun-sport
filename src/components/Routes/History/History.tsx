import jwtDecode from 'jwt-decode';
import React from 'react';
import { HiOutlineArrowLeft } from "react-icons/hi";
import { useNavigate } from 'react-router';
import { useTheme } from 'styled-components';
import { AuthContext } from '../../Context/AuthContext';
import BottomTabBar from '../../Shared/BottomTabBar';
import { Desc, ItemContainer, ItemTitle, Title } from './historyStyledComp';
import { onValue, ref } from '@firebase/database';
import { database } from '../../../firebase-config';

const History = (): JSX.Element => {

  const [history, setHistory] = React.useState([]);
  const { user } = React.useContext(AuthContext);
  const navigate = useNavigate();
  const theme: any = useTheme();

  React.useEffect(() => {
    let email;

    if (user) {
      const jwt = jwtDecode<any>(user);
      email = jwt.email.substring(0, jwt.email.indexOf('@'));
    }
    const db = ref(database, `history/${ email }`);

    let retrieved: any = [];

    onValue(db, (snapshot) => {

      snapshot.forEach((snap) => {
        retrieved.push(snap.val());
      });
      setHistory(retrieved);

    }, {
      onlyOnce: true
    });
  }, [user]);

  return (
    <>
      <div style={{
        margin: 20,
        marginTop: 40,
      }}>
        <div>
          <HiOutlineArrowLeft
            onClick={() => navigate('/home')}
            size={30}
            color={theme.text}
          />
        </div>
        <div>
          <Title>History</Title>
          <Desc style={{color: theme.lightText}}>
            Lorem ipsum dolor sit amet, 
            consectetur adipiscing elit.
          </Desc>
        </div>
        <div style={{overflow: 'scroll', height: 500}}>
          {
            history.map((item: any, index: any) => (
              <div key={index}>
                <ItemContainer style={{background: theme.background}}>
                  <ItemTitle>
                    { item.sportName }
                  </ItemTitle>
                  <img
                    height={80}
                    width="80%"
                    src={item.sportImgURL}
                    alt="no_img"
                    style={{
                      borderRadius: 25,   
                    }}
                  />
                  <img
                    height={20}
                    src={
                      item.reaction === 'like'
                      ? require('../../../assets/images/heart_blue.png')
                      : require('../../../assets/images/dislike.png')
                    }
                    alt="no_img"
                    style={{
                      alignSelf: 'center',
                      margin: '0 auto',     
                    }}
                  />
                </ItemContainer>
              </div>
            ))
          }
        </div>
      </div>

      <BottomTabBar />
    </>
  )
}

export default History;