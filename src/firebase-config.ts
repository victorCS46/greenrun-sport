import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCgrFrhMMrKH6f1V3GaPWik5cPMhnvDjr0",
  authDomain: "greenrun-sport-app.firebaseapp.com",
  projectId: "greenrun-sport-app",
  storageBucket: "greenrun-sport-app.appspot.com",
  messagingSenderId: "602352653137",
  appId: "1:602352653137:web:0331a18bef39e827c73970",
  measurementId: "G-VCRD9CRGW7"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const database = getDatabase(app);